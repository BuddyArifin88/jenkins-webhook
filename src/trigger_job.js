'use strict';

const req = require('supertest');
const env = require('dotenv').config();
const api = req(process.env.JENKINS_AUTH);
const url = process.env.JENKINS_URL;
const token = process.env.TOKEN;

function getListJenkinsJobs() {
  return api.get(`/api/json?pretty=true`)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json');
}

function runJob(url) {
  return api.post(url)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json');
}

async function constructJobAndTrigger(repoName) {
  await runJob(`${repoName}build?token=${token}`);
}

function triggerJob(branch,  featureJobList, developJobList) {
  const isFeatureJob = new RegExp('^(.*feature|.*hotfix|.*story|.*bugs)(?!.*sdet).*$');
  const isDevelopJob = new RegExp('(?:^|\b)(develop)(?=\b|$)');

  if(isFeatureJob.test(branch)) {
    featureJobList.forEach(repoName => {
      console.log(`[${new Date()}] ${branch} >>> ${featureJobList} >>> ${isFeatureJob.test(branch)}`);
      constructJobAndTrigger(repoName);
    })
  } else if(isDevelopJob.test(branch)) {
    developJobList.forEach(repoName => {
      console.log(`[${new Date()}] ${branch} >>> ${developJobList} >>> ${isDevelopJob.test(branch)}`);
      constructJobAndTrigger(repoName);
    })
  }
}

async function startTrigger(payload) {
  let urlDevelop = [];
  let urlFeature = [];

  const service = payload.repository.name
    .replace('-', '')
    .split('-')[0]
    .toUpperCase();

  const branch = payload.push.changes[0].new.name;
  const response = await getListJenkinsJobs();

  for(const job of response.body.jobs) {
    const jenkinsJob = job.name.replace('-', '')
      .split('-')[0]
      .toUpperCase();

    if (jenkinsJob === service) {
      if(job.name.toUpperCase().includes('DEVELOP')) {
        const path = job.url.replace(url, '');
        urlDevelop.push(path);
      }

      if(job.name.toUpperCase().includes('FEATURE')) {
        const path = job.url.replace(url, '');
        urlFeature.push(path);
      }
    }
  }

  await triggerJob(branch, urlFeature, urlDevelop);
};

module.exports = {
  startTrigger,
};
