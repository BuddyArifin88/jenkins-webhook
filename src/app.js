const express = require('express');
const bodyParser = require('body-parser');
const triggerJob = require('./trigger_job');

const app = express();
app.use(bodyParser.json());

app.listen(12000, () => { console.log('started port 12000')})

// GET Request
app.get('/', (req, res) => res.send('Hello World!'))

app.post('/trigger', async (req, res) => {
  const payload = req.body;
  const branch = payload.push.changes[0].new.name;
  const repo = payload.repository.name
    .replace('-', '')
    .split('-')[0]
    .toUpperCase();

  console.log(`[${new Date()}] Build Job >> branch ${branch} repo ${repo}`);
  await triggerJob.startTrigger(payload);
  res.send({ post: `Success` });
});

module.exports = app;
