FROM node:8.11.4-alpine

RUN npm upgrade --global yarn
RUN npm -v
RUN yarn global add npm@6.1.0
RUN npm -v

RUN mkdir -p sample-api
COPY . sample-api/
RUN ls sample-api

WORKDIR sample-api
RUN npm install --verbose

EXPOSE 12000

CMD ["npm", "run", "start-service"]